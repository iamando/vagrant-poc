# Vagrant Nginx API's Gateway for Docker

Nginx API's Gateway for Docker on Vagrant

## Commands

```bash
# up
vagrant up

# status
vagrant status


# destroy
vagrant destroy
```
## Configurations

```bash
# connect on dev machine via ssh
vagrant ssh deb

# install curl, nginx, docker
sudo apt-get update && sudo apt-get install -y curl nginx
curl -L get.docker.com | sudo bash 

# edit docker service to bind docker daemon to a TCP socket
sudo nano /lib/systemd/system/docker.service 

# replace line 13 to append this
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock -H tcp://127.0.0.1:2375

# restart services
sudo systemctl daemon-reload
sudo systemctl restart docker

# you can test it
sudo docker run -d hello-world
curl localhost:2375/v1.41/images/json

# configure the Nginx redirect from port 80 to 2375
echo ' ' > /etc/nginx/sites-enabled/default
sudo nano /etc/nginx/sites-enabled/default # use the nginx.conf on repo to replace it

# restart nginx service
sudo systemctl restart nginx

# test
curl localhost
curl -s localhost/images/json
```
## API's Docs

```bash
# pull image
curl -X POST "http://localhost/image/create?fromImage=alpine:latest"

# create container instance
curl "http://localhost/containers/create" -X POST  -H "Content-Type: application/json"  -d '{"Image": "alpine", "Cmd": ["echo", "hello world"]}'

# start container
curl -X POST http://localhost/containers/105afe8209/start

# list containers
curl -X POST http://localhost/containers/json

# list images
curl -X POST http://localhost/images/json

# stop container
curl -X POST http://localhost/containers/105afe8209/stop

# print log of container
curl -X POST http://localhost/containers/105afe8209/logs?stdout=1
```
## Support

This Project is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers.

## License

[MIT licensed](LICENSE).
